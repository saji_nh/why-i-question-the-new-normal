files = Dir.glob("*.html.md.erb")
files.each do |fname|
  `html-beautify  --config .jsbeautifyrc -r --templating erb --good-stuff -E [p,span] #{fname.chomp}`
end
