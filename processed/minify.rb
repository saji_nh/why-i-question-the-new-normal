files = Dir.glob("../source/en/views/*.html.md.erb")
files.each do |fname|
  p fname
  start=false
  arr=[]
  yml=[]
  IO.readlines(fname).each do |line|
  start=!start if line.chomp=="---"
  yml<<line if start
  unless start
    line.chomp!
      arr<<line unless line.empty?
    end
  end
  yml<<arr.shift

  local_fname = File.basename(fname)

  txt=""
  txt+=arr.shift+"\n"
  txt+=arr.join("")
  File.open("#{local_fname}","w") do |fout|
   fout.puts yml
   fout.puts txt
  end
end

